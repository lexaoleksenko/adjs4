let list = fetch("https://ajax.test-danit.com/api/swapi/films")

list
    .then((res) => res.json())
    .then((data) => {
        data.forEach(e => {
            let elList = document.createElement("ul")
            let elName = document.createElement("li")
            let elEpis = document.createElement("li")
            let elDesc = document.createElement("li")

            elEpis.textContent = e.episodeId
            elName.textContent = e.name
            elDesc.textContent = e.openingCrawl

                e.characters.forEach(e=>{
                let characters = fetch(e)
                characters
                        .then((txt) => txt.json())
                        .then((data) => {
                            let p = document.createElement("p")
                            p.textContent = data.name;
                            elList.append(p)
                        })
            })

            document.body.append(elList);
            elList.append(elEpis)
            elList.append(elName)
            elList.append(elDesc)
        })
    })